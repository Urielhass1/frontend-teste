## Instruções:

1. Faça um fork deste repositório;
2. Implemente o HTML/CSS da tela com base no layout disponível;
3. Preocupe-se em desenvolver o css utilizando mobile first;
4. Para a interação do formulário utilize preferencialmente angularjs e/ou jQuery;
5. Após terminar seu teste submeta um pull request e aguarde feedback.

## Faça login no Zeplin para visualizar o frontend
* https://app.zeplin.io/
* Login: facedigital
* Senha: facedigital@private
* Projeto: https://app.zeplin.io/project/59b7ec30a09e2bcccf9bbd10

### Você pode:

* Utilizar qualquer linguagem de preprocessador css ou css puro;
* Utilizar um task runner de sua preferência;
* Utilizar bibliotecas css como compass, bourbon, animatecss ou outras;
* Utilizar componentes do bower.

### Esperamos que você:

* Minifique seu css e deixe-o na pasta "css";
* Minifique seu javascript e deixe-o na pasta "js";
* Faça commit também dos arquivos não minificados;
* Dê suporte a IE10+, Chrome, Safari e Firefox.

* **Importante:** Usamos o mesmo teste para todos os níveis de front: **junior**, **pleno** ou **senior**, mas procuramos adequar nossa exigência na avaliação com cada um desses níveis sem, por exemplo, exigir excelência de quem está começando :-)

### Ganhe pontos extras por:

* Desenvolver HTML semântico;
* Utilizar boas práticas de SEO;
* Utilizar '@font-face' para os ícones;
* Utilizar componentização do seu css;
* Ser fiel as especificações apresentadas;
* Validar os inputs do seu formulário antes de habilitar o botão de envio;
* Utilizar animações para o scroll da página.

### Breakpoints:

| Nome do breakpoint | Largura mínima | Descrição                         |
|--------------------|----------------|-----------------------------------|
| phone              | 320px          | Breakpoint para smartphones       |
| tablet             | 768px          | Breakpoint para tablets           |
| desktop            | 1024px         | Breakpoint para desktops comuns   |
| monitor            | 1280px         | Breakpoints para desktops grandes |