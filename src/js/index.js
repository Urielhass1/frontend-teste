var $ = jQuery;
$(document).ready(function(){

	$(".form input").on("change", function(event){
		var form = $(".form input");
		var button = $(".form button");
		var checkbox = $(".form-check-input");
		var pass = $("#password").val();
		var confirmpass = $("#confirmPassword").val();

		var self = $(this);

		if(checkbox.prop( "checked" )){
			button.removeAttr('disabled');
		}else{
			button.attr('disabled', 'disabled');
		}

		if(pass != confirmpass){
			button.attr('disabled', 'disabled');
			if(self.attr('id') == "password" || self.attr('id') == 'confirmPassword'){
				if(pass != "" && confirmpass != "") alert("As senhas não conferem.");
			}
		}

		form.each(function(){
			var self = $(this);
			if(self.val() == "" && self.attr("type") != "checkbox"){
				button.attr('disabled', 'disabled');
				event.stopPropagation();
				return false;
			}
		})
	})

	$(".form button[type='button']").on("click", function(ev){
		var self = $(this);
		if(!self.attr('disabled')){
			var contact = [];
			var i = 0;
			$(".form input").each(function(){
				var input = $(this);
				var name = input.attr("name");
				if(input.attr("type") != "checkbox"){
					//contact[i] = { name : input.val() };
					contact[name] = input.val();
				}else{
					contact[name] = true ;
				}
				i++;
			})

			$.ajax({
	            url: "php/insert.php",
	            type: "POST",
	            data: $(".form").serialize(),
	            success: function( data ){
	                alert( data );
	                $(".form input").each(function(){
	                	$(this).val("");
	                })
	                $("#terms").prop('checked', false); 
	            },
	            error: function(){
	                alert('ERRO');
	            }
	        });
		}
		ev.stopPropagation();
	})
})