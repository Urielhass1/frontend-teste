<?php
require_once('config.php');

if(!empty($_POST)){

    $sql = 'INSERT INTO contact (';
    $values = ' VALUES (';
    $first = true;

    unset($_POST['confirmPassword']);
    
    if(key_exists("terms", $_POST)){
        $_POST['terms'] = 1;
    }

    foreach($_POST as $key => $value):
        if($first){
            $sql .= "{$key}";
            $values .= ":{$key}";
            $first = false;
        }else{
            $sql .= ", {$key}";
            $values .= ", :{$key}";
        }

    endforeach;

    $sql .= ") ";
    $values .= ')';

    $sql .= $values;

    try {
        $query = $bd->prepare($sql);
        
        foreach($_POST as $key => $value):
            $name = ":{$key}";
            $query->bindValue($name , $value, is_numeric($value) ? PDO::PARAM_INT : PDO::PARAM_STR);
        endforeach;

        if($query->execute()){
            echo "Dados inseridos com sucesso";
        }else{
            echo "Falha na inserção de dados";
        }

    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
?>